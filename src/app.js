const fs = require("fs");
const path = require("path");
const http = require("http");
const PrismatikConnection = require("./PrismatikConnection.js");
const LightState = require("./LightState.js");

class App{
	constructor(){
		this.configPath = path.resolve(__dirname, "../config.json");
		this.config = null;
		this.onStopCbs = [];
		this.initialPrismatikConnectionDone = false;
		this.onPrismatikRecvCbs = [];
		this.prismatikConnection = null;
		this.stopGetLightsLoop = false;
		this.onGetLightsLoopStoppedCbs = [];
		this.httpServer = null;
		this.prismatikIsCapturing = false;
		this.isSettingLights = false;
		this.storedLightStates = new Map();
	}

	start(){
		if(!fs.existsSync(this.configPath)){
			console.log("no config.json found, copy config.sample.json to config.json and configure your settings.");
			this.fireStopCbs();
			return;
		}

		let configStr = fs.readFileSync(this.configPath);
		let config = JSON.parse(configStr);

		this.config = {...{
			prismatikIp: "127.0.0.1",
			prismatikPort: 3636,
			lightMap: [],
			httpServer: true,
			httpServerPort: 8272,
		}, ...config}

		this.prismatikConnection = new PrismatikConnection(this.config.prismatikPort, this.config.prismatikIp, this.config.prismatikApiKey);

		if(this.config.httpServer){
			this.httpServer = http.createServer(async (request, response) => {
				if(request.method == "GET" && request.url == "/start"){
					try{
						let {success, error} = await this.enablePrismatik();
						if(success){
							this.serverResponse(response, 200, "started");
						}else{
							this.serverResponse(response, 500, error);
						}
					}catch(e){
						this.serverResponse(response, 500, e.toString());
					}
				}else if(request.method == "GET" && request.url == "/stop"){
					try{
						let {success, error} = await this.disablePrismatik();
						if(success){
							this.serverResponse(response, 200, "stopped");
						}else{
							this.serverResponse(response, 500, error);
						}
					}catch(e){
						this.serverResponse(response, 500, e.toString());
					}
				}else{
					this.serverResponse(response, 404);
				}
			}).listen(this.config.httpServerPort);
		}
	}

	serverResponse(response, code, text){
		response.writeHead(code, {"Content-Type": "text/plain"});
		if(text) response.write(text);
		response.end();
	}

	onStop(cb){
		this.onStopCbs.push(cb);
	}

	async requestStop(){
		await this.disablePrismatik();
		if(this.httpServer){
			this.httpServer.close();
		}
		this.fireStopCbs();
	}

	fireStopCbs(){
		for(const cb of this.onStopCbs){
			cb();
		}
	}

	async enablePrismatik(){
		if(this.prismatikIsCapturing) return {
			success: false,
			error: "Failed to start, already running.",
		};
		console.log("starting prismatik");
		let success = await this.setPrismatikStatus("on");
		if(!success){
			console.log("Failed to start");
			return {
				success: false,
				error: "Failed to start, is prismatik not running?",
			}
		}
		this.prismatikIsCapturing = true;
		this.getLightsLoop();
		if(!this.isSettingLights){
			this.isSettingLights = true;
			this.setLightsLoop();
		}
		console.log("prismatik started");
		return {success: true};
	}

	async disablePrismatik(){
		if(!this.prismatikIsCapturing) return {
			success: false,
			error: "Failed to stop, already stopped."
		};
		console.log("stopping prismatik");
		this.prismatikIsCapturing = false;
		await this.setPrismatikStatus("off");
		this.stopGetLightsLoop = true;
		await new Promise(r => this.onGetLightsLoopStoppedCbs.push(r));
		console.log("prismatik stopped");
		return {success: true};
	}

	async setPrismatikStatus(status){
		if(!this.prismatikConnection.connected) return false;
		let lockResult = await this.prismatikConnection.sendCmd("lock");
		if(!lockResult || lockResult.indexOf("lock:success") < 0) return false;
		let setstatusResult = await this.prismatikConnection.sendCmd("setstatus:"+status);
		if(!setstatusResult) return false;
		let unlockResult = await this.prismatikConnection.sendCmd("unlock");
		if(!unlockResult) return false;
		return true;
	}

	async getLightsLoop(){
		while(true){
			if(this.stopGetLightsLoop || !this.prismatikConnection.connected){
				this.stopGetLightsLoop = false;
				this.prismatikIsCapturing = false;
				for(const cb of this.onGetLightsLoopStoppedCbs){
					cb();
				}
				return;
			}
			let result = await this.prismatikConnection.sendCmd("getcolors");
			if(!result) continue;
			if(result.startsWith("colors:")){
				let colorsStr = result.substring(7);
				let splitColors = colorsStr.split(";");
				for(const color of splitColors){
					let match = color.match(/(\d+)-(\d+),(\d+),(\d+)/);
					if(match){
						let lightId = parseInt(match[1]);
						let r = parseInt(match[2]);
						let g = parseInt(match[3]);
						let b = parseInt(match[4]);
						let deviceId = this.config.lightMap[lightId];
						if(deviceId){
							let state = this.storedLightStates.get(deviceId);
							if(!state){
								state = new LightState();
								this.storedLightStates.set(deviceId, state);
							}
							state.setRgb(r,g,b);
						}
					}
				}
			}
		}
	}

	async setLightsLoop(){
		while(true){
			if(!this.prismatikIsCapturing){
				this.isSettingLights = false;
				return;
			}

			let didAny = false;
			for(const [deviceId, state] of this.storedLightStates){
				let {putData, needsUpdate} = state.getPutData();
				if(needsUpdate){
					this.setLightState(deviceId, putData);
					await this.wait(100);
					didAny = true;
				}
			}
			if(!didAny){
				await this.wait(100);
			}
		}
	}

	async wait(ms){
		await new Promise(r => setTimeout(r, ms));
	}

	async setLightState(deviceId, putData){
		return await this.putRequest(`http://${this.config.hubIp}/gateways/${deviceId}`, JSON.stringify(putData));
	}

	async putRequest(url, putData){
		await new Promise((resolve, reject) => {
			let req = http.request(url, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Content-Length": putData.length,
				}
			}, res => {
				let body =  "";
				res.on("data", chunk => {
					body += chunk;
				});
				res.on("end", _ => {
					resolve(body);
				});
			});
			req.on("error", reject);
			req.write(putData);
			req.end();
		});
	}
}
module.exports = App;
