const LightStateParam = require("./LightStateParam.js");

class LightState{
	constructor(){
		this.hue = new LightStateParam("hue", val => val *360);
		this.saturation = new LightStateParam("saturation", val => val * 100);
		this.value = new LightStateParam("brightness", val => val * 255);
		this.onState = new LightStateParam("state", val => val ? "On" : "Off");

		this.params = [this.hue, this.saturation, this.value, this.onState];
	}

	setRgb(r,g,b){
		let {h,s,v} = this.RGBtoHSV(r,g,b);
		if(s < 0.1) h = 0;
		this.hue.setRealVal(h);
		this.saturation.setRealVal(s);
		this.value.setRealVal(v);
		this.onState.setRealVal(v > 0.06 ? 1 : 0);
	}

	getPutData(){
		let needsUpdate = false;
		let putData = {};
		for(const param of this.params){
			if(param.modifyPutData(putData)){
				needsUpdate = true;
			}
		}
		return {putData, needsUpdate};
	}

	//https://stackoverflow.com/a/17243070/3625298
	RGBtoHSV(r, g, b) {
		if (arguments.length === 1) {
			g = r.g, b = r.b, r = r.r;
		}
		var max = Math.max(r, g, b), min = Math.min(r, g, b),
			d = max - min,
			h,
			s = (max === 0 ? 0 : d / max),
			v = max / 255;

		switch (max) {
			case min: h = 0; break;
			case r: h = (g - b) + d * (g < b ? 6: 0); h /= 6 * d; break;
			case g: h = (b - r) + d * 2; h /= 6 * d; break;
			case b: h = (r - g) + d * 4; h /= 6 * d; break;
		}

		return {
			h: h,
			s: s,
			v: v
		};
	}
}

module.exports = LightState;
