class LightStateParam{
	constructor(paramName, modifierFn){
		this.paramName = paramName;
		this.modifierFn = modifierFn;
		this.realVal = 0;
		this.lastSentVal = 0;
	}

	setRealVal(val){
		this.realVal = val;
	}

	modifyPutData(putData){
		if(Math.abs(this.realVal - this.lastSentVal) > 0.1){
			this.lastSentVal = this.realVal;
			putData[this.paramName] = this.modifierFn(this.realVal);
			return true;
		}
		return false;
	}
}

module.exports = LightStateParam;
