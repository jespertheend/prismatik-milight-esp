const service = require("os-service");
const App = require("./app.js");

if(process.argv[2] == "--addService"){
  service.add("prismatik-milight-esp", error => {
  	if(error){
  		console.trace(error);
  	};
  });
}else if(process.argv[2] == "--removeService"){
	service.remove("prismatik-milight-esp", error => {
		if(error){
			console.trace(error);
		}
	});
}else{
	let app = new App();
	app.onStop(_ => {
		service.stop();
	});
	app.start();
	service.run(async _ => {
		console.log("service stop requested, stopping app");
		app.requestStop();
	});
	process.on("SIGINT", _ => {
		console.log("sigint, stopping app");
		app.requestStop();
	});
}
