const net = require("net");

class PrismatikConnection{
	constructor(ip, port, apiKey){
		this.ip = ip;
		this.port = port;
		this.apiKey = apiKey;
		this.socket = null;
		this.onRecvCbs = [];
		this.createSocket(ip, port, apiKey);
		this.connectionFailedPermanently = false;
		this.connected = false;
	}

	createSocket(){
		this.socket = new net.Socket();
		this.socket.connect(this.ip, this.port, async _ => {
			let connectResult = await this.sendCmd();
			console.log("connected to prismatik");
			let result = await this.sendCmd("apikey:"+this.apiKey);
			if(!result.startsWith("ok")){
				console.log("failed to connect to prismatik, invalid api key?");
				this.connectionFailedPermanently = true;
			}else{
				this.connected = true;
			}
		});
		this.socket.on("data", data => {
			for(const cb of this.onRecvCbs){
				cb(data.toString());
			}
			this.onRecvCbs = [];
		});
		this.socket.on("error", e => {
			if(!this.connected){
				console.log("error connecting to prismatik, make sure it's running and you have enabled the server api in the experimental tab");
			}else{
				console.log("prismatik socket error", e);
			}
		});
		this.socket.on("close", e => {
			console.log("Prismatik socket closed, will retry in 30 seconds");
			this.socket.destroy();
			this.socket = null;
			for(const cb of this.onRecvCbs){
				cb(null);
			}
			this.onRecvCbs = [];
			this.connected = false;
			setTimeout(this.createSocket.bind(this), 30000);
		});
	}

	async sendCmd(cmd){
		let promise = new Promise(r => this.onRecvCbs.push(r));
		if(cmd)	this.socket.write(cmd+"\n");
		return await promise;
	}
}

module.exports = PrismatikConnection;
